% -*- coding: utf-8 -*-
\documentclass[a4paper,ps2pdf,9pt]{beamer}
\usepackage{slides}

\title{Brief journey in the big world of Integer Programming with the Knapsack}

\author{\href{http://www.opimedia.be/}{Olivier \textsc{Pirson}}}
\institute{Presentation for the oral exam of\\
  INFO-F424 \textit{Combinatorial optimization}\\[3ex]
  Computer Science Department\\
  Université Libre de Bruxelles\\[1ex]
  \href{http://www.ulb.ac.be/}{\includegraphics[height=0.75cm]{img/ULB}}
}
\date{June \nth{1}, 2017\\[-1ex]
  {\tiny(Cosmetic correction November 26, 2017)}\\
  \href{https://bitbucket.org/OPiMedia/brief-journey-in-the-big-world-of-integer-programming-with-the/}{Last version: \path|{https://bitbucket.org/OPiMedia/}|}\\
  \href{https://bitbucket.org/OPiMedia/brief-journey-in-the-big-world-of-integer-programming-with-the/}{\path|{brief-journey-in-the-big-world-of-integer-programming-with-the/}|}}

\hypersetup{pdfsubject={Presentation for the oral exam of INFO-F424 Combinatorial optimization}}
\hypersetup{pdfkeywords={combinatorial optimization,branch and bound,linear relaxation,knapsack problem}}

\logo{\includegraphics[width=1cm]{img/Discrete_Optimization__Expontential__th}}


\hyphenpenalty=10000
\sloppy

\begin{document}
\title{{\Huge\textit{Brief journey in the big world of\\
      Integer Programming\\
      with the\\[1ex]
      Knapsack}}}
\begin{frame}[plain]
  \null\hspace*{-2.5em}%
  \parbox{\textwidth}{\titlepage}%
\end{frame}
\title{\textit{Brief journey\dots{} with the Knapsack}}
\author{}
\institute{}
\date{}

\AtBeginSection{
  \begin{frame}
    \tableofcontents[currentsection]
  \end{frame}
}



\section{Problem}
\begin{frame}[t]
  \frametitle{$0-1$ Knapsack Problem formulation}

  Let
  \begin{itemize}
  \item
    a set of $n$ items, for each $i \in \{1, 2, \dots, n\}$
    \begin{itemize}
    \item
      a weight $a_i$
    \item
      a value $c_i$
    \end{itemize}
  \item
      a knapsack of capacity $b$
  \end{itemize}

  \bigskip
  Find the subset of $\{1, 2, \dots, n\}$\\
  that it may be contained in the knapsack
  \textit{and} maximizes the value.

  \bigskip
  The canonical Binary Integer Program formulation:

  Variables:
  $x_i \in \{0, 1\},
  x_i = \left\{
  \begin{array}[u]{@{\ }ll}
    1 & \text{if item $i$ is selected}\\
    0 & \text{otherwise}
  \end{array}\right.$

  \medskip
  Constraint: $\sum\limits_{i=1}^n a_i x_i \leq b$

  \medskip
  Objective function: $\max \sum\limits_{i=1}^n c_i x_i$
\end{frame}



\section{Primal and dual bounds}
\begin{frame}[t]
  \frametitle{Greedy heuristic algorithm -- primal/lower bound}
  Sort variables by decreasing order of density
  $\frac{\text{value}}{\text{weight}}$:
  $\frac{c_i}{a_i}$

  \bigskip
  In this order, pick each possible item.\\
  That fill the knapsack with the best \textit{independent} choice.

  \bigskip
  For example:\\
  $\begin{array}{lr@{\ }r@{\ }r@{\ }r@{\ }r@{\ }r}
   \max & 45 x_1 & + & 48 x_2 & + & 9 x_3\\
   \text{subject to} & 5 x_1 & + & 12 x_2 & + & 3 x_3 & \leq 16\\
   \hline
   \text{density} & 9 & & 4 & & 3\\
  \end{array}$

  \medskip
  Give the solution: $(1, 0, 1)$
  with value $45 + 9 = 54$.

  \medskip
  $54$ is a lower bound of the problem.

  \smallskip
  Indeed $(0, 1, 1)$ is a better solution (in fact the optimal solution
  \footnote{
    In simple example like this\\
    it is very easy to perform an exhaustive search.})\\
  of value $48 + 9 = 57$.
\end{frame}


\begin{frame}[t]
  \frametitle{Linear relaxation -- dual/upper bound}
  The idea is to permit piece of item, and then fill completely the knapsack.
  Of course the relaxed problem is an other problem
  and its solution is not a solution of the initial problem.

  But this solution is an upper bound for the initial problem.

  \bigskip
  With the same example:\\
  $\begin{array}{lr@{\ }r@{\ }r@{\ }r@{\ }r@{\ }r}
   \max & 45 x_1 & + & 48 x_2 & + & 9 x_3\\
   \text{subject to} & 5 x_1 & + & 12 x_2 & + & 3 x_3 & \leq 16\\
  \end{array}$
  \begin{picture}(0,0)
    \put(50,-40){\includegraphics[width=3cm]{img/chocolat}}
  \end{picture}

  and now $x_i \in [0, 1]$ instead $\in \{0, 1\}$

  \bigskip
  $5 + \alpha 12 = 16
  \Longrightarrow
  \alpha = \frac{11}{12}$

  Give $(1, \frac{11}{12}, 0)$
  with value $45 + \frac{11}{12} 48 = 89$.

  \bigskip
  We known now that the optimal solution $x^{\star}$ of the initial problem
  is such that:
  $54 \leq x^{\star} \leq 89$

  \bigskip
  We obtained theses two bound in $O(n)$.
\end{frame}


\begin{frame}[t]
  \frametitle{General principle of relaxation}
  For the Integer Programming\\
  $z = \max \{c(x) \mid x \in X \subseteq R^n\}$\\

  \medskip
  $z^R = \max \{f(x) \mid x \in T \subseteq R^n\}$\\
  is a relaxation
  if
  \begin{itemize}
  \item
    $X \subseteq T$
  \item
    $c(x) \leq f(x)\qquad\forall x \in X$
  \end{itemize}
  \begin{picture}(0,0)
    \put(150,-20){\includegraphics[width=3cm]{img/relaxation}}
  \end{picture}

  \bigskip
  \bigskip
  So $z^R$ is an upper bound for the initial problem.
\end{frame}



\section{Branch and Bound}
\begin{frame}
  \frametitle{Exhaustive search}
  We can break the problem into two subproblems, and so forth.

  That give this kind of binary tree representation:
  \begin{figure}
    \includegraphics[width=0.7\textwidth]{img/exhaustive}\\
    \caption{\hyperlink{MOOC}{\textit{Discrete Optimization}}}
  \end{figure}
\end{frame}


\begin{frame}[t]
  \frametitle{Contain the Exponential Explosion}

  There is $2^n$ possibilities,
  so an exhaustive search is quickly impossible.

  \medskip
  In fact, it is a NP-hard problem.\\
  (The decision problem is already NP-complete.)

  \bigskip
  The goal is to push as far as possible the exponential curve.
  \begin{figure}
    \includegraphics[width=0.2\textwidth]{img/Discrete_Optimization__Expontential_1}
    \qquad
    \includegraphics[width=0.2\textwidth]{img/Discrete_Optimization__Expontential_2}
    \qquad
    \includegraphics[width=0.2\textwidth]{img/Discrete_Optimization__Expontential_3}\\
    \caption{\hyperlink{MOOC}{\textit{Discrete Optimization}}}
  \end{figure}

  How avoid the impossible (in practice) exhaustive search?
\end{frame}


\begin{frame}
  \frametitle{Depth-First Branch and Bound}
  Pruning\dots
  \begin{figure}
    \includegraphics[width=0.7\textwidth]{img/branch_1}
    \caption{\hyperlink{MOOC}{\textit{Discrete Optimization}}}
  \end{figure}

  We have the optimal solution $80$ with only $7$ nodes visited,\\
  instead $2^4 - 1 = 15$.
\end{frame}



\section{References}
\begin{frame}
  \frametitle{References}
  References:
  \small
  \begin{itemize}
  \item\phantomsection\label{MOOC}
    Pascal \textsc{Van Hentenryck}.\\
    \textbf{\textit{Discrete Optimization}}.\\
    MOOC, University of Melbourne,
    \href{https://www.coursera.org/learn/discrete-optimization}{https://www.coursera.org/learn/discrete-optimization}

  \item\label{Wiley 1998}
    Laurence A. \textsc{Wolsey}.\\
    \textbf{\textit{Integer Programming}}.\\
    Wiley, 1998
  \end{itemize}

  \begin{figure}
    \includegraphics[width=0.6\textwidth]{img/xkcd_np_complete}\\
    {\tiny
      \href{https://xkcd.com/287/}{\textit{NP-Complete}, xkcd}}
  \end{figure}
\end{frame}

\end{document}
