.. -*- restructuredtext -*-

=========================================================================
*Brief journey in the big world of Integer Programming with the Knapsack*
=========================================================================
Presentation for the oral exam of INFO-F424 *Combinatorial optimization* (ULB):

* Primal and dual bounds
* Linear relaxation
* Branch and Bound



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



|knapsack|

.. |knapsack| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/13/2375314656-2-brief-journey-in-the-big-world-of-int_avatar.png
